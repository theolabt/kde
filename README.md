KDE
===
<img src=".README/KDE_loader.gif"  width="35%" height="35%" alt="KDE full logo loader">

KDE is a game engine of my making. It's made to allow the automated management of images, sounds, entities and events for any game that'll be created using the game engine.

It's currently in development and made mostly to support 2D games.

> using monogame

# UML Schema

currently at the 0.6.0 version, the engine can support entities interacting with each other through events and Caracteristics inside a specified scene. It's possible and easy to switch form one scene to another. <br>
Outside Inputs from the user are easily added to interact with the scenes and entities.

<img src=".README/UML.png"  width="50%" height="50%" alt="KDE UML schema">


# Next update to come

- A rework of the Event system is necessary to make the progression easier.
- Images, animation and the loading of assets is close to be finished.
- Adding sound and a sound manager is a must to come.
